import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Platform, Dimensions, TouchableOpacity
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Spinner from 'react-native-loading-spinner-overlay';
import Input from '../../components/input';
import ValidationRules from '../../utils/forms/ValidationRules';
import { signIn } from '../../store/actions/UserActions';

class LoginForm extends Component {
    state = {
      loading: false,
      spinner: false,
      hasErrors: false,
      errorMessage: '',
      type: 'Login',
      action: 'Login',
      form: {
        email: {
          value: '',
          valid: false,
          type: 'textinput',
          rules: {
            isRequired: true,
          }
        },
        password: {
          value: '',
          valid: false,
          type: 'textinput',
          rules: {
            isRequired: true,
          }
        }
      }
    }

    formHasErrors = () => (
      this.state.hasErrors
        ? (
          <View style={styles.errorContainer}>
            <Text style={styles.errorLabel}>Oops, check your info.</Text>
          </View>
        )
        : null
    );


    updateInput = (name, value) => {
      this.setState({
        hasErrors: false
      });

      const formCopy = this.state.form;
      formCopy[name].value = value;

      // /rules
      const { rules } = formCopy[name];
      const valid = ValidationRules(value, rules);

      formCopy[name].valid = valid;

      this.setState({
        form: formCopy
      });
    }

    manageAccess = () => {
      console.log('user Token', this.props.User.auth.token);

      if (!this.props.User.auth.token) {
        this.setState({ hasErrors: true });
        alert('Invalid credentials');
      } else {
        console.log('AuthToken', this.props.User);
        this.props.goHome();
        // setTokens(this.props.User.auth.token,()=>{
        //     this.setState({hasErrors:false});

        // })
      }
    }

    createUser = () => {
      this.props.goNext();
    }

    resetPassword = () => {
      this.props.goPassword();
    }

    submitUser = () => {
      this.setState({
        loading: true,
        spinner: true
      });
      let isFormValid = true;
      const formToSubmit = {};
      const formCopy = this.state.form;

      for (const key in formCopy) {
        if (this.state.type === 'Login') {
          // LOGIN

          isFormValid = isFormValid && formCopy[key].valid;
          formToSubmit[key] = formCopy[key].value;
        } else {
          // REGISTER
          isFormValid = isFormValid && formCopy[key].valid;
          formToSubmit[key] = formCopy[key].value;
        }
      }

      if (isFormValid) {
        if (this.state.type === 'Login') {
          this.props.signIn(formToSubmit).then((response) => {
            console.log('response SignIn', response);

            if (response.payload.token) {
              // this.manageAccess()
              this.setState({
                spinner: false
              });
              // storeData(response.payload.token);
              this.props.goHome();
            } else {
              this.setState({
                spinner: false
              }).then(() => {
                alert('Invalid credentials');
              });
            }
          });
        } else {
          this.props.signUp(formToSubmit).then(() => {
            this.manageAccess();
          });
        }
      } else {
        this.setState({
          hasErrors: true,
          spinner: false
        });
        alert('Email and password are required');
      }
    }


    render() {
      return (
        <View style={styles.inputWrapper}>
          <Spinner
            visible={this.state.spinner}
            textContent="SignIn..."
            color="#F63"
            textStyle={styles.spinnerTextStyle}
          />
          <Input
            placeholder="Email"
            placeholderTextColor="#cecece"
            type={this.state.form.email.type}
            value={this.state.form.email.value}
            autoCapitalize="none"
            keyboardType="email-address"
            onChangeText={(value) => this.updateInput('email', value)}
          />

          <Input
            placeholder="Password"
            placeholderTextColor="#cecece"
            type={this.state.form.password.type}
            value={this.state.form.password.value}
            onChangeText={(value) => this.updateInput('password', value)}
                    // overrideStyle={{}}
            secureTextEntry
          />

          {/* {this.confirmPassword()}
                {this.formHasErrors()} */}

          <View style={styles.buttonWrapper}>
            <TouchableOpacity style={styles.button} onPress={this.submitUser}>
              <Text style={styles.buttonText}>Log In</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.horizontalLineWrapper}>

            <View style={styles.hairline} />
            <Text style={styles.loginButtonBelowText1}>Or</Text>
            <View style={styles.hairline} />
          </View>
          <View style={styles.buttonWrapper}>
            <TouchableOpacity style={[styles.button, styles.createAccountButton]} onPress={this.createUser}>
              <Text style={styles.createAccountButtonText}>Create an Account</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.forgotPwdWrapper}>
            <View style={styles.forgotPwd}>
              <TouchableOpacity onPress={this.resetPassword}>
                <Text style={styles.forgotPwdText}>Forgot Your Password?</Text>
              </TouchableOpacity>

            </View>
          </View>


        </View>

      );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  errorContainer: {
    marginBottom: 10,
    marginTop: 30,
    padding: 10,
    backgroundColor: '#f44336'
  },
  errorLabel: {
    color: '#fff',
    textAlignVertical: 'center',
    textAlign: 'center'
  },
  button: {
    ...Platform.select({
      ios: {
        marginBottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f63',
        height: 40,
        width: DEVICE_WIDTH - 80,
        marginTop: 10,
        borderRadius: 5,
        color: 'white',

      },
      android: {
        marginBottom: 10,
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f63',
        height: 40,
        width: DEVICE_WIDTH - 80,
        borderRadius: 5,
        color: 'white',
      }
    })
  },
  inputWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonWrapper: {
    display: 'flex'
  },
  buttonText: {
    color: 'white'
  },
  createAccountButton: {
    backgroundColor: 'white',
  },
  buttonText: {
    color: 'white'
  },
  createAccountButtonText: {
    color: '#f63'
  },
  forgotPwdWrapper: {
    marginTop: 40
  },
  forgotPwdText: {
    color: '#fff'
  },
  forgotPwd: {
    ...Platform.select({
      ios: {
        marginBottom: 0,
      },
      android: {
        marginBottom: 10,
        marginTop: 10,
      }
    })
  },
  hairline: {
    backgroundColor: 'white',
    height: 1,
    width: 130
  },

  loginButtonBelowText1: {
    fontFamily: 'AvenirNext-Bold',
    fontSize: 14,
    paddingHorizontal: 5,
    alignSelf: 'center',
    color: 'white',
    position: 'relative',
    top: -10
  },
  horizontalLineWrapper: {
    flexDirection: 'row',
    marginTop: 30
  },
  spinnerTextStyle: {
    color: '#f63'
  },
});


function mapStateToProps(state) {
  console.log('state token', state.User);
  return {
    User: state.User
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ signIn }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);

import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Dimensions, TouchableOpacity, ScrollView, Image, KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  osName, osVersion, mobileModel,
} from 'react-device-detect';
import HBTextInput from '../../components/HBTextInput';
import LoginLogo from './LoginLogo';
import ValidationRules from '../../utils/forms/ValidationRules';
import { signUp } from '../../store/actions/UserActions';


class SignupForm extends Component {
    static navigationOptions = ({ navigation }) => ({

      title: '',
      headerTitleStyle: { fontWeight: 'normal', fontSize: 16 },
      headerLeft: (
        <TouchableOpacity onPress={() => {
          navigation.navigate(
            'Auth',
          );
        }}
        >
          <View style={{
            alignItems: 'center', flexDirection: 'row', justifyContent: 'center', borderBottomWidth: 0
          }}
          >
            <Image source={require('../../assets/images/arrowBack.png')} style={{ width: 25, height: 25, marginLeft: 25, }} />
            {/* <Text allowFontScaling={false} style={{color: 'black', fontSize: 17}}>Back</Text> */}
          </View>
        </TouchableOpacity>
      ),
      headerStyle: {
        borderBottomColor: 'transparent',
        borderBottomWidth: 0,
        shadowColor: 'transparent',
        shadowOpacity: 0,
        shadowOffset: { height: 0, },
        shadowRadius: 0,
        elevation: 0,
      },

      gesturesEnabled: false,
      animationEnabled: false,
    });

    state = {
      hasErrors: false,
      errorMessage: '',
      type: 'Signup',
      action: 'Signup',
      form: {
        email: {
          value: '',
          valid: false,
          type: 'textinput',
          rules: {
            isRequired: true,
          }
        },
        password: {
          value: '',
          valid: false,
          type: 'textinput',
          rules: {
            isRequired: true,
          }
        }
      },

    }

    updateInput = (name, value) => {
      this.setState({
        hasErrors: false
      });

      const formCopy = this.state.form;
      formCopy[name].value = value;

      // /rules
      const { rules } = formCopy[name];
      const valid = ValidationRules(value, rules, formCopy);

      formCopy[name].valid = valid;

      this.setState({
        form: formCopy
      });
    }

    submitUser = () => {
      const formCopy = this.state.form;
      const formToSubmit = {
        user: {
          email: this.state.form.email.value,
          password: this.state.form.password.value,
          user_profile_attributes: {
            address_attributes: {
              address1: '',
              city: '',
              state: '',
              country: 'US'
            }
          }
        },
        device_type: 'Phone',
	        device_name: mobileModel,
	        device_version: osVersion,
	        operating_system: osName
      };
        // console.log("signup data",formToSubmit);
      if (this.state.form.email.valid && this.state.form.password.valid) {
        if (this.state.form.password.value.length < 8) {
          alert('Password is too short (minimum is 8 characters)');
        } else {
          this.props.signUp(formToSubmit).then((response) => {
            if (response.payload.token) {
              this.props.navigation.navigate('HomePage');
            } else if (response.payload.status == 400) {
              alert('User already registered');
            } else {
              alert('Invalid email');
            }
          });
        }
      } else {
        alert('Email and password are required');
        this.setState({
          hasErrors: true
        });
      }
    }

    manageAccess = () => {
      if (!this.props.User.auth.token) {
        this.setState({ hasErrors: true });
        alert('Email is invalid');
      } else {
        this.props.navigation.navigate('Home');
        // setTokens(this.props.User.auth.token,()=>{
        //     this.setState({hasErrors:false});
        //     //this.props.goNext();
        // })
      }
    }

    render() {
      return (
        <ScrollView style={styles.scrollview}>
          <KeyboardAvoidingView behavior="padding">
            <View style={styles.logoWrapper}>
              <LoginLogo />
            </View>

            <View style={styles.inputWrapper}>

              <HBTextInput
                placeholder="Email"
                placeholderTextColor="#cecece"
                type={this.state.form.email.type}
                value={this.state.form.email.value}
                autoCapitalize="none"
                keyboardType="email-address"
                onChangeText={(value) => this.updateInput('email', value)}
              />

              <HBTextInput
                placeholder="Password"
                placeholderTextColor="#cecece"
                type={this.state.form.password.type}
                value={this.state.form.password.value}
                onChangeText={(value) => this.updateInput('password', value)}

                secureTextEntry
              />
              <View style={styles.buttonWrapper}>
                <TouchableOpacity style={styles.button} onPress={this.submitUser}>
                  <Text style={styles.buttonText}>Create an Account</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
// const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width, // for full screen
    height: Dimensions.get('window').height, // for full screen
  },
  button: {
    marginBottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f63',
    height: 40,
    width: DEVICE_WIDTH - 80,
    marginTop: 15,
    borderRadius: 5,
    color: 'white',
  },
  inputWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonWrapper: {
    display: 'flex'
  },
  buttonText: {
    color: 'white'
  },

  logoWrapper: {
    display: 'flex',
    marginTop: 120
  },
  scrollview: {
    // backgroundColor: transpare
  },
});


function mapStateToProps(state) {
  console.log('Signup Return', state);
  return {
    User: state.User
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ signUp }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupForm);

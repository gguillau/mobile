import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Dimensions, TouchableOpacity, ScrollView, Image, KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HBTextInput from '../../components/HBTextInput';
import LoginLogo from './LoginLogo';
import { forgotPassword } from '../../store/actions/UserActions';
import ValidationRules from '../../utils/forms/ValidationRules';


class ForgotPwd extends Component {
    static navigationOptions = ({ navigation }) => ({

      title: '',
      headerTitleStyle: { fontWeight: 'normal', fontSize: 16 },
      headerLeft: (
        <TouchableOpacity onPress={() => {
          navigation.navigate(
            'Auth',
          );
        }}
        >
          <View style={{
            alignItems: 'center', flexDirection: 'row', justifyContent: 'center', borderBottomWidth: 0
          }}
          >
            <Image source={require('../../assets/images/arrowBack.png')} style={{ width: 25, height: 25, marginLeft: 25, }} />
            {/* <Text allowFontScaling={false} style={{color: 'black', fontSize: 17}}>Back</Text> */}
          </View>
        </TouchableOpacity>
      ),
      headerStyle: {
        borderBottomColor: 'transparent',
        borderBottomWidth: 0,
        shadowColor: 'transparent',
        shadowOpacity: 0,
        shadowOffset: { height: 0, },
        shadowRadius: 0,
        elevation: 0,
      },
      gesturesEnabled: false,
      animationEnabled: false,
    });

    state = {
      hasErrors: false,
      errorMessage: '',
      type: 'Password',
      action: 'Password',
      form: {
        email: {
          value: '',
          valid: false,
          type: 'textinput',
          rules: {
            isRequired: true,
          }
        }
      }
    }

    updateInput = (name, value) => {
      this.setState({
        hasErrors: false
      });

      const formCopy = this.state.form;
      formCopy[name].value = value;

      // /rules
      const { rules } = formCopy[name];
      const valid = ValidationRules(value, rules, formCopy);

      formCopy[name].valid = valid;

      this.setState({
        form: formCopy
      });
    }

    submitUser = () => {
      const isFormValid = true;
      let formToSubmit = {};
      const formCopy = this.state.form;

      if (this.state.form.email.valid) {
        formToSubmit = {
          user: {
            email: this.state.form.email.value
          }
        };
        this.props.forgotPassword(formToSubmit).then((response) => {
          console.log('pwd response', response.payload);
          if (response.payload == '') {
            alert('Please check your email for a link to update your password.');
          } else {
            alert(response.payload);
          }
        });
      } else {
        alert('Email is required');
        this.setState({
          hasErrors: true
        });
      }
    }

    manageAccess = () => {
      alert('Please check your email for a link to update your password.');
      // if(!this.props.User.auth.token){
      //     this.setState({hasErrors:true})
      //     alert("Email is invalid");
      // } else {
      //     setTokens(this.props.User.auth.token,()=>{
      //         this.setState({hasErrors:false});
      //         //this.props.goNext();
      //     })
      // }
    }

    render() {
      return (
        <ScrollView style={styles.scrollview}>
          <KeyboardAvoidingView behavior="padding">
            <View style={styles.logoWrapper}>
              <LoginLogo />
            </View>
            <View style={styles.inputWrapper}>

              <HBTextInput
                placeholder="Email"
                placeholderTextColor="#cecece"
                type={this.state.form.email.type}
                value={this.state.form.email.value}
                autoCapitalize="none"
                keyboardType="email-address"
                onChangeText={(value) => this.updateInput('email', value)}
              />
              <View style={styles.buttonWrapper}>
                <TouchableOpacity style={styles.button} onPress={this.submitUser}>
                  <Text style={styles.buttonText}>Reset your password </Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width, // for full screen
    height: Dimensions.get('window').height // for full screen
  },
  button: {
    marginBottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f63',
    height: 40,
    width: DEVICE_WIDTH - 80,
    marginTop: 15,
    borderRadius: 5,
    color: 'white',
  },
  inputWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonWrapper: {
    display: 'flex'
  },
  buttonText: {
    color: 'white'
  },

  logoWrapper: {
    display: 'flex',
    marginTop: 120
  }
});


function mapStateToProps(state) {
  console.log(state);
  return {
    User: state.User
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ forgotPassword }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPwd);

import React, { Component } from 'react';
import {
  StyleSheet, View, ScrollView, ActivityIndicator, ImageBackground, Dimensions, KeyboardAvoidingView
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LoginLogo from './LoginLogo';
import LoginForm from './LoginForm';
import { autoSignIn } from '../../store/actions/UserActions';
import {
  getTokens, setTokens, getData
} from '../../api/Helper';
import backgroundImage from '../../assets/images/apartmentCabinetContemporary1027508.png';


class LoginScreen extends Component {
  state = {
    loading: true
  }

  goNext = () => {
    this.props.navigation.navigate('SignUp');
  }

  goPassword = () => {
    this.props.navigation.navigate('Password');
  }

  goHome = () => {
    console.log('props', this.props);
    this.props.navigation.navigate('HomePage');
  }

  componentDidMount() {
    // storeData("");
    getData().then((response) => {
      if (response != null) {
        this.goHome();
      } else {

      }
    });

    getTokens((value) => {
      if (value[0][1] === null) {
        this.setState({ loading: false });
      } else {
        this.props.autoSignIn(value[1][1]).then(() => {
          if (!this.props.User.auth.token) {
            this.setState({ loading: false });
          } else {
            setTokens(this.props.User.auth, () => {
              this.goNext();
            });
          }
        });
      }
    });
  }


  render() {
    if (this.state.loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <ScrollView style={styles.scrollview}>
        <ImageBackground style={[styles.container, styles.fixed, { zIndex: -1 }]} source={backgroundImage}>
          <KeyboardAvoidingView behavior="padding">
            <View style={styles.logoWrapper}>
              <LoginLogo />

            </View>
            <View>
              <LoginForm
                goNext={this.goNext}
                goPassword={this.goPassword}
                goHome={this.goHome}
              />
            </View>
          </KeyboardAvoidingView>
        </ImageBackground>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width, // for full screen
    height: Dimensions.get('window').height // for full screen
  },
  loading: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  picture: {
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'cover',
    height: '100%',
    width: '100%'
  },
  fixed: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollview: {
    backgroundColor: 'transparent'
  },
  logoWrapper: {
    display: 'flex',
   // marginTop: 50
  }
});


function mapStateToProps(state) {
  return {
    User: state.User
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ autoSignIn }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

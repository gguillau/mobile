import React from 'react';
import {
  View, Image, Text, StyleSheet
} from 'react-native';
import LogoImage from '../../assets/images/logo.png';


const LoginLogo = () => (
  <View style={{ alignItems: 'center' }}>
    <Image
      source={LogoImage}
      style={styles.logo}
    />
    <Text style={styles.logoText}>A Homeowner's Best Friend</Text>
  </View>
);

const styles = StyleSheet.create({
  logo: {
    width: 100,
    height: 120,
    // marginBottom: 50
  },
  logoText: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 20,
  },
});

export default LoginLogo;

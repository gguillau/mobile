import React, { Component } from 'react';
import {
  View, StyleSheet, Dimensions, TouchableOpacity, ScrollView, 
  Image, Text, ImageBackground, TextInput, Switch, Picker
} from 'react-native';
import backgroundImage from '../../assets/images/apartmentCabinetContemporary1027508.png';
import HBTextInput from '../../components/HBTextInput';

export default class SignupForm extends Component {
    static navigationOptions = ({ navigation }) => ({

        headerLeft: (
            <TouchableOpacity onPress={() => {
              navigation.navigate(
                'HomePage',
              );
            }}
            >
              <View style={{
                alignItems: 'center', flexDirection: 'row', justifyContent: 'center', borderBottomWidth: 0
              }} >
                <Image source={require('../../assets/images/arrowBack.png')} 
                style={{ width: 25, height: 25, marginLeft: 25, }} />
              </View>
            </TouchableOpacity>
          ),
     
        headerRight:(
            <TouchableOpacity onPress={() => {
            ""
            }}
            >
            <View style={{
                alignItems: 'center', flexDirection: 'row', justifyContent: 'center', borderBottomWidth: 0
            }}
            >
                <Text allowFontScaling={false} style={{color: 'black', fontSize: 17, marginRight:25}}>Save</Text>
            </View>
            </TouchableOpacity>
        ),

        gesturesEnabled: false,
        animationEnabled: false,
    });


    render() {
      return (
        <ScrollView style={styles.scrollview}>
            <ImageBackground source={backgroundImage} style={ styles.container }>
                <Text style={ styles.bannerText }>New Binder</Text>  
                <Text style={[styles.bannerText, styles.bannerSubText ] }>For Additional Properties</Text>  
             </ImageBackground>
            
            <View style = { styles.binderWraper}> 
                
                <View style={[ styles.flexColumn, styles.fBox, styles.fBoxWidth]}>
                    <Text>Home Name</Text>
                    <TextInput style={ styles.inputStyle } placeholder={'Home'}></TextInput>
                </View>
                <View style={[ styles.flexColumn, styles.fBox, styles.fBoxWidth]}>
                    <Text>Address</Text>
                    <TextInput style={ styles.inputStyle } placeholder={'123 Main Street'}></TextInput>
                </View>
                <View style={[ styles.flexRow, styles.fBox, styles.fBoxWidth]}>
                    <View style={[ styles.flexColumn, styles.fBoxRowWidth]}>
                        <Text>Apt/Unit #</Text>
                        <TextInput style={ styles.inputStyle } placeholder={'Apt. 2'}></TextInput>
                    </View>
                    <View style={[ styles.flexColumn, styles.fBoxRowWidth, styles.fBoxRowWSpace]}>
                        <Text>City</Text>
                        <TextInput style={ styles.inputStyle } placeholder={'Boston'}></TextInput>
                    </View>
                </View>
                <View style={[ styles.flexRow, styles.fBox, styles.fBoxWidth]}>
                    <View style={[ styles.flexColumn, styles.fBoxRowWidthD3]}>
                        <Text>State</Text>
                        <TextInput style={ styles.inputStyle } placeholder={'Apt. 2'}></TextInput>
                    </View>
                    <View style={[ styles.flexColumn, styles.fBoxRowWidthD3, styles.fBoxRowWSpaceD3]}>
                        <Text>Postal Code</Text>
                        <TextInput style={ styles.inputStyle } placeholder={'Boston'}></TextInput>
                    </View>
                    <View style={[ styles.flexColumn, styles.fBoxRowWidthD3, styles.fBoxRowWSpaceD3]}>
                        <Text>Country</Text>
                        {/* <TextInput style={ styles.inputStyle } placeholder={'Boston'}></TextInput> */}
                        <Picker>
  <Picker.Item label="Java" value="java" />
  <Picker.Item label="JavaScript" value="js" />
</Picker>
                    </View>
                </View>
                {/* <View style={[ styles.flexRow, styles.fBox, styles.fBoxWidth, styles.switchWrapper]}>
                    <View style={[ styles.flexColumn, styles.fBoxRowWidth]}>
                        <Text>Default Binder</Text>
                    </View>
                    <View style={[ styles.flexColumn, styles.fBoxRowWidth]}>
                        <Switch style={[ styles.switchStyle ]}></Switch>
                    </View>
                </View> */}
            </View>
        </ScrollView>
      );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
// const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    resizeMode:"contain",
    //alignItems:"center",
    justifyContent:"center",
    height:100
  },
  scrollview: {
   display:"flex"
    // backgroundColor: transpare
  },
  bannerText:{
      
  },
  binderWraper:{
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    marginTop:50,
    padding:10,
   
  },
  inputStyle:{
    width:'100%',
    borderBottomWidth:1,
    fontSize:16,
    padding:5,
    marginTop:10,
    //width: DEVICE_WIDTH - 80,
    height:25,
    borderBottomColor:'#f63'
  },
  flexColumn:{
    display: "flex",
    alignItems: "flex-start",
    flexDirection: "column"
  },
  flexRow:{
    display: "flex",
    alignItems: "flex-start",
    flexDirection: "row"
  },
  fBox:{
    marginTop:15
  },
  fBoxWidth:{
    width:"100%"
  },
  fBoxRowWidth:{
    width:"48%"
  },
  fBoxRowWSpace:{
    marginLeft:"4%"
  },
  fBoxRowWidthD3:{
    width:"32%"
  },
  fBoxRowWSpaceD3:{
    marginLeft:"2%"
  },
  switchWrapper:{
      marginTop:20
  },
  switchStyle:{
      height:20,
      
  },
  bannerText:{
    display:"flex",
    paddingLeft:20,
    alignItems:"flex-start",
    justifyContent:"flex-start",
    color:"white",
    //fontWeight:"bold",
    fontSize:24
},
bannerSubText:{
    fontSize:12,
    paddingTop:5,
    paddingLeft:20,
},
});

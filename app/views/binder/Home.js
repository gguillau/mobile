import React, { Component } from 'react';
import {
  View, StyleSheet, Dimensions, TouchableOpacity, ScrollView, Image, Text
} from 'react-native';
import backgroundImage from '../../assets/images/apartmentCabinetContemporary1027508.png';

 export default class SignupForm extends Component {
    static navigationOptions = ({ navigation }) => ({
     
      headerRight:(
        <TouchableOpacity onPress={() => {
         ""
        }}
        >
          <View style={{
            alignItems: 'center', flexDirection: 'row', justifyContent: 'center', borderBottomWidth: 0
          }}
          >
            <Image source={require('../../assets/images/add.png')} style={{ width: 20, height: 20, marginRight: 25, }} />
            {/* <Text allowFontScaling={false} style={{color: 'black', fontSize: 17, marginRight:25}}>Save</Text> */}
          </View>
        </TouchableOpacity>
      ),

      gesturesEnabled: false,
      animationEnabled: false,
    });


    render() {
      return (
        <ScrollView style={styles.scrollview}>
          <Image source={backgroundImage} style={ styles.container } />
        </ScrollView>
      );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
// const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    height:500
  },
  scrollview: {
    flex: 1,
    marginLeft:40,
    marginRight:40,
    marginTop:40,
    // backgroundColor: transpare
  },
});


// function mapStateToProps(state){
//     console.log(state)
//     return {
//         User: state.User
//     }
// }

// function mapDispatchToProps(dispatch){
//     return bindActionCreators({signUp},dispatch);
// }

// export default connect(mapStateToProps,mapDispatchToProps)(SignupForm);

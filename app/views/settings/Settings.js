import React, { Component } from 'react';
import {
  View, StyleSheet, Dimensions, TouchableOpacity, ScrollView, 
  Image, Text, ImageBackground, TextInput, Switch
} from 'react-native';
import User from '../../assets/images/user.png';
import Email from '../../assets/images/mail.png';
import Password from '../../assets/images/password.png';
import LogOut from '../../assets/images/logout.png';


export default class SignupForm extends Component {

    render() {
      return (
        <ScrollView style={styles.scrollview}>
            <View style={ styles.container }>
                <Text style={styles.headingText}>SETTINGS</Text>
             </View>
             <View style={styles.settingsWraper}>
                <View style={styles.flexRow}>
                    <TouchableOpacity style={styles.fBoxWidth}>
                    <Image source={ User } style={styles.icon } />
                    <Text allowFontScaling={false} style={ styles.iconText }>My Account</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity style={styles.fBoxWidth}>
                    <Image source={ Email } style={styles.icon } />
                    <Text allowFontScaling={false} style={ styles.iconText }>Change Email</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity style={styles.fBoxWidth}>
                    <Image source={ Password } style={styles.icon } />
                    <Text allowFontScaling={false} style={ styles.iconText }>Change Password</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.flexRow}>
                    <TouchableOpacity style={styles.fBoxWidth}>
                    <Image source={ LogOut } style={styles.icon } />
                    <Text allowFontScaling={false} style={ styles.iconText }>Logout</Text>
                    </TouchableOpacity>
                </View>
             </View>
            
           
        </ScrollView>
      );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
// const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    height:35,
    backgroundColor:"grey"
  },
  headingText:{
      display:"flex",
      padding :10,
      alignItems:"flex-start",
      justifyContent:"flex-start",
      color:"white",
      fontWeight:"bold"
  },
  settingsWraper:{
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    marginTop:30,
    padding:10
  },
  icon:{
    width: 20, 
    height: 20, 
    flexDirection:"row", 
    padding: 10
  },
  iconText:{
    color: 'black', 
    fontSize: 17, 
    flexDirection:"row", 
    marginLeft:15
  },
  flexRow:{
    display: "flex",
    alignItems: "flex-start",
    flexDirection: "row",
    marginBottom:30
  },
  fBoxWidth:{
    width:"100%",
    flexDirection:"row"
  },
  
});

import React from 'react';
import { StyleSheet, View } from 'react-native';
import { RootNavigator } from './routes';

const Nav = RootNavigator();

const App = () => (
  <View style={styles.container}>
    <Nav />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});


export default App;

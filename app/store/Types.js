export const SIGN_IN = 'sign_in';
export const SIGN_UP = 'sign_up';
export const AUTO_SIGN_IN = 'auto_sign_in';
export const FORGOT_PASSWORD = 'forgot_password';
import { createStackNavigator, createAppContainer, 
  createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';
import React from "react"
import { Image } from "react-native"
//import Ionicons from 'react-native-vector-icons/Ionicons';

// SCREENS
import SignIn from './views/login/LoginScreen';
import SignUp from './views/login/SignupForm';
import ForgotPwd from './views/login/ForgotPwd';
import HomeScreen from './views/binder/Home';
import Binder from "./views/binder/Form"
import Settings from "./views/settings/Settings"
import HomeIcon from "./assets/images/home.png"
import settingsIcon from "./assets/images/settings.png"


const headerConf = {
  headerLayoutPreset:'center',
  defaultNavigationOptions:{
      headerStyle:{
          backgroundColor:"white"
      },
      headerTintColor:'Black',
      headerTitle: (<Image style={{ width: 30, height: 35 }} 
      source={require('./assets/images/logo.png')}/>)
  }
}

const AuthStack = createStackNavigator({
  SignIn
}, {
  headerMode: 'none'
});
const SignupStack = createStackNavigator({
  SignUp
}, {

});
const PasswordStack = createStackNavigator({
  Password: ForgotPwd
}, {
  // headerMode: 'none'
});
const HomeStack = createStackNavigator({
  HomePage: HomeScreen
}, headerConf);

const BinderStack = createStackNavigator({
  Binder: Binder
}, headerConf);
const SettingsStack = createStackNavigator({
  Settings: Settings
}, headerConf);

const BottomStack = createBottomTabNavigator({
  HomePage:HomeStack,
  Binder:BinderStack
},{
  tabBarOptions:{
      activeTintColor:'#fff',
      showLabel:false,
      activeBackgroundColor:"white",
      inactiveBackgroundColor:'white',
      barStyle: { backgroundColor: '#694fad' },
      style: {
          backgroundColor: 'white',
      }
  },
  initialRouteName:'HomePage',
  defaultNavigationOptions:({navigation})=>({
      tabBarIcon:({focused,horizontal,tintColor})=>{
          const  { routeName } = navigation.state;
          let iconName;
          if(routeName === 'HomePage'){
            return <Image style={{ width: 25, height: 25 }} source={HomeIcon}/>;
          } else if(routeName === 'Binder'){
            return <Image style={{ width: 25, height: 25 }} source={settingsIcon}/>;
          }

         
      }
  })
});

export const RootNavigator = () => createAppContainer(createSwitchNavigator({
  Auth: AuthStack,
  SignUp: SignupStack,
  Password: PasswordStack,
  HomePage: HomeStack,
  Binder: BinderStack,
  Bottom: BottomStack,
  Settings: SettingsStack
}, {
  initialRouteName: 'Binder'
}));
